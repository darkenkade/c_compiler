#!/usr/bin/python3
import sys, os, re
from enum import Enum

symbols = {'{': '{', '}': '}', '(': '\(', ')': '\)', ';': ';'}
keywords = {'int': 'int', 'return': 'return', 'identifier': '[a-zA-Z]\w*'}
int_literals = {
    'integer': '[0-9]*',
    'hex': '0x[a-zA-Z0-9]+',
    'octal': '0[0-9]+'
}
token_tuple = (symbols, keywords, int_literals)
token_key = ('symbol', 'keyword', 'literal')
tokens = []


class Token:
    def __init__(self, token, token_type):
        self.token = token
        self.token_type = token_type

    def __repr__(self):
        return f"{self.token} : {self.token_type}"


def is_valid_token(token):
    for regex_dict in token_tuple:
        for re_key in regex_dict:
            if re.match("^"+regex_dict[re_key]+"$", token):
                return Token(token, re_key)
    return None


def is_symbol(token):
    return token in symbols


def add_token(token):
    token = is_valid_token(token)
    if token is not None:
        tokens.append(token)
        return True
    else:
        return False


def lex(source):
    token = ""
    space = " "
    new_line = "\n"
    i = 0
    while i < len(source):
        if source[i] != space and source[i] != new_line:
            if source[i].isalnum():
                token += source[i]
                while i + 1 < len(source) and source[i + 1].isalnum():
                    i += 1
                    token += source[i]
                if add_token(token):
                    token = ""
        if is_symbol(source[i]):
            tokens.append(Token(source[i], symbols[source[i]]))
        i += 1

    for x in tokens:
        print(x.token, end=" ")
    print()


assembly_format = """
    .globl main
main:
    movl ${},%eax
    ret
"""

source_file = sys.argv[1]
assembly_file = os.path.splitext(source_file)[0] + ".s"

with open(source_file, 'r') as infile, open(assembly_file, 'w') as outfile:
    source = infile.read().strip()
    lex(source)
